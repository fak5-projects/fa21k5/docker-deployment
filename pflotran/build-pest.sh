#!/usr/bin/env bash
set -e

git clone https://gitlab.com/petsc/petsc petsc
cd petsc
git checkout "$PESTC_VERSION"
./configure --CFLAGS='-O3' \
            --CXXFLAGS='-O3' \
            --FFLAGS='-O3' \
            --with-debugging=no \
            --download-hdf5=yes \
            --download-mpich=yes \
            --download-fblaslapack=yes \
            --download-metis=yes \
            --download-parmetis=yes
PESTC_DIR=$(realpath .)
export PESTC_DIR
cd "$PESTC_DIR"
export PESTC_ARCH=arch-linux2-c-opt
make all -j "$(nproc)"
cd ..
