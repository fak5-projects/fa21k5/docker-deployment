#!/usr/bin/env bash
set -e

git clone https://bitbucket.org/pflotran/pflotran
PETSC_DIR=$(realpath ./petsc)
export PETSC_DIR
export PETSC_ARCH=arch-linux2-c-opt
cd pflotran/src/pflotran
git checkout "$PFLOTRAN_VERSION"
make pflotran -j "$(nproc)"