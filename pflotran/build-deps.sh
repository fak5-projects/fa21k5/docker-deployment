#!/usr/bin/sh
set -e

export DEBIAN_FRONTEND=NONINTERACTIVE
apt-get update -y
apt-get install -y git gcc gfortran g++ cmake python
